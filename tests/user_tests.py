import pytest
import json
from api.models import Client


@pytest.fixture
def user_data():
    return [
        {
            "phone": "1635489463",
            "operator_code": "554",
            "tag": "Female",
            "gmt_timezone": -3
        },
        {
            "phone": "1060027755",
            "operator_code": "110",
            "tag": "Rich",
            "gmt_timezone": -9
        },
    ]


@pytest.mark.django_db
def test_get_user(client, user_data):
    url = '/client/'
    Client.objects.bulk_create([Client(**data) for data in user_data])

    response = client.get(url)

    assert response.status_code == 200
    assert len(response.json()) == 2


@pytest.mark.django_db
def test_create_user(client, user_data):
    url = '/client/create'
    payload = user_data[0]
    response = client.post(url, data=payload)

    assert response.status_code == 200
    assert Client.objects.count() == 1
    assert response.json() == {'instance_info': payload}


@pytest.mark.django_db
def test_update_user(client, user_data):
    initial_payload = user_data[0]
    updated_payload = initial_payload.copy()
    updated_payload['tag'] = 'Updated Tag'

    user = Client.objects.create(**initial_payload)
    assert Client.objects.count() == 1

    url = f'/client/update/{user.id}'
    print("!"*10)
    print(updated_payload)
    print(type(updated_payload))
    print(json.dumps(updated_payload))
    print(type(json.dumps(updated_payload)))

    response = client.put(url, data=json.dumps(updated_payload))
    assert response.status_code == 200
    assert response.json() == {'instance_info': updated_payload}

    user.refresh_from_db()
    assert user.tag == 'Updated Tag'


@pytest.mark.django_db
def test_delete_user(client, user_data):
    payload = user_data[0]

    user = Client.objects.create(**payload)
    assert Client.objects.count() == 1

    url = f'/client/delete/{user.id}'
    response = client.delete(url)

    assert response.status_code == 200
    assert not Client.objects.exists()
