import pytest
import pytz
import json
from dateutil.parser import isoparse
from api.models import Broadcast
from django.conf import settings
from datetime import datetime


tz = pytz.timezone(settings.TIME_ZONE)


@pytest.fixture
def broadcast_data():
    return [
        {
            "start_time": "2023-05-04T11:09:26.662+06:00",
            "text": "zovjnnm qKubyS xCiKlo ahkupcLN uGrwxZffHW thjvPn klexYCajb",
            "tag_filter": "Female",
            "op_code_filter": "301",
            "end_time": "2023-08-29T11:09:26.662+06:00"
        },
        {
            "start_time": "2023-06-17T11:09:28.356+06:00",
            "text": "GbktlSOqf pWfzItysV OPFchMrL QjMnLuYw dKoWmiC",
            "tag_filter": "Poor",
            "op_code_filter": "110",
            "end_time": "2023-09-04T11:09:28.356+06:00"
        }
    ]


@pytest.fixture
def broadcast_field_names():
    return [field.name for field in Broadcast._meta.get_fields()]


@pytest.mark.django_db
def test_get_broadcast(client, broadcast_data, broadcast_field_names):
    url = '/broadcast/'
    Broadcast.objects.bulk_create([Broadcast(**data) for data in broadcast_data])
    response = client.get(url)

    assert response.status_code == 200
    assert len(response.json()) == 2


@pytest.mark.django_db
def test_create_broadcast(client, broadcast_data):
    url = '/broadcast/create'
    payload = broadcast_data[0]
    response = client.post(url, data=payload)

    assert response.status_code == 200
    assert Broadcast.objects.count() == 1
    assert type(Broadcast.objects.first().start_time) == datetime
    assert Broadcast.objects.first().messages is not None
    assert response.json() == {'instance_info': payload}


@pytest.mark.django_db
def test_update_broadcast(client, broadcast_data):
    initial_payload = broadcast_data[0]
    updated_payload = initial_payload.copy()
    updated_payload['text'] = 'Updated Text'

    broadcast = Broadcast.objects.create(start_time=isoparse(initial_payload['start_time']),
                                         text=initial_payload['text'],
                                         tag_filter=initial_payload['tag_filter'],
                                         op_code_filter=initial_payload['op_code_filter'],
                                         end_time=isoparse(initial_payload['end_time']))
    assert Broadcast.objects.count() == 1
    assert type(broadcast.start_time) != str
    assert type(broadcast.end_time) != str

    url = f'/broadcast/update/{broadcast.id}'
    response = client.put(url, data=json.dumps(updated_payload))
    assert response.status_code == 200
    assert response.json() == {'instance_info': updated_payload}

    broadcast.refresh_from_db()
    assert broadcast.text == 'Updated Text'


@pytest.mark.django_db
def test_delete_broadcast(client, broadcast_data):
    payload = broadcast_data[0]

    broadcast = Broadcast.objects.create(start_time=isoparse(payload['start_time']),
                                         text=payload['text'],
                                         tag_filter=payload['tag_filter'],
                                         op_code_filter=payload['op_code_filter'],
                                         end_time=isoparse(payload['end_time']))
    url = f'/broadcast/delete/{broadcast.id}'
    response = client.delete(url)

    assert response.status_code == 200
    assert not Broadcast.objects.exists()


@pytest.mark.django_db
def test_get_broadcast_stats(client, broadcast_data):
    broadcast_id = 1
    url = f'/broadcast/stats/{broadcast_id}'
    payload = broadcast_data[0]

    broadcast = Broadcast.objects.create(start_time=isoparse(payload['start_time']),
                                         text=payload['text'],
                                         tag_filter=payload['tag_filter'],
                                         op_code_filter=payload['op_code_filter'],
                                         end_time=isoparse(payload['end_time']))

    response = client.get(url)

    assert response.status_code == 200
    assert response.json() == {
        'broadcast': {
            'id': broadcast.id,
            'start_time_str': payload['start_time'],
            'text': payload['text'],
            'tag_filter': payload['tag_filter'],
            'op_code_filter': payload['op_code_filter'],
            'end_time_str': payload['end_time'],
        },
        'message_count': 0,
        'sent_message_count': 0,
    }
