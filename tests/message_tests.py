import pytest
from api.models import Message, Broadcast, Client
import requests

@pytest.fixture
def broadcast_data():
    return {
            "start_time": "2022-07-11T15:19:11Z",
            "text": "HI2",
            "tag_filter": "Rich",
            "op_code_filter": "111",
            "end_time": "2025-08-23T15:22:23Z"
        }

@pytest.fixture
def user_data():
    return [
        {
            "phone": "1635489463",
            "operator_code": "999",
            "tag": "Rich",
            "gmt_timezone": -3
        },
        {
            "phone": "1060027755",
            "operator_code": "111",
            "tag": "Poor",
            "gmt_timezone": -9
        },
    ]


@pytest.mark.django_db
def test_get_messages(client, broadcast_data, user_data):
    Client.objects.bulk_create([Client(**data) for data in user_data])
    Broadcast.objects.create(**broadcast_data)

    assert Broadcast.objects.count() == 1
    assert Client.objects.count() == 2

    broadcast = Broadcast.objects.first()
    assert broadcast.messages.exists()
    assert broadcast.messages.count() == 2

@pytest.mark.django_db
def test_external_api(client, user_data, broadcast_data):
    Client.objects.bulk_create([Client(**data) for data in user_data])
    Broadcast.objects.create(**broadcast_data)

    message = Message.objects.first()
    url = f'https://probe.fbrq.cloud/v1/send/{message.id}'
    body_data = {
        "id": message.id,
        "phone": message.recieving_client.phone,
        "text": message.broadcast.text
    }
    jwt_token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MjE4MDU0MjgsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6Imh0dHBzOi8vdC5tZS9rdWJhbmVtaWwifQ.SJEv_9GHwsNS0KeND7yMuj1l9Sf4jh8C1KEmXACV3KM'
    headers = {
        "Authorization": f"Bearer {jwt_token}",
        "Content-Type": "application/json"
    }
    response = requests.post(url, json=body_data, headers=headers)
    assert response.status_code == 200
    for key in response.json().keys():
        assert key in ['code', 'message']
    assert response.json()['message'] == 'OK'
    assert message.is_sent