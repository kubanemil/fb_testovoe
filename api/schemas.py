from ninja import schema
from datetime import datetime
from pydantic import BaseConfig

BaseConfig.arbitrary_types_allowed = True


class BroadcastSchema(schema.Schema):
    id: int
    start_time_str: datetime
    text: str
    tag_filter: str
    op_code_filter: str
    end_time_str: datetime


class BroadcastInputSchema(schema.Schema):
    start_time: datetime
    text: str
    tag_filter: str
    op_code_filter: str
    end_time: datetime


class BroadcastUpdateSchema(schema.Schema):
    start_time: datetime
    text: str
    tag_filter: str
    op_code_filter: str
    end_time: datetime
    
    
class ClientSchema(schema.Schema):
    id: int
    full_phone_number: str
    operator_code: str
    tag: str
    gmt_timezone: int


class ClientInputSchema(schema.Schema):
    phone: str
    operator_code: str
    tag: str
    gmt_timezone: int


class ClientUpdateSchema(schema.Schema):
    phone: str = None
    operator_code: str = None
    tag: str = None
    gmt_timezone: int = None


class ModelReturnSchema(schema.Schema):
    instance_info: dict


class ErrorSchema(schema.Schema):
    message: str = 'Some error...'
    error: str = 'Unknown error.'


class BroadcastStatsSchema(schema.Schema):
    broadcast: dict
    message_count: int
    sent_message_count: int


class MessageSchema(schema.Schema):
    time_str: datetime
    is_sent: bool
    broadcast_id: int
    client_id: int