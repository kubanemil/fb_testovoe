import requests
import pytz
from datetime import datetime
from django.conf import settings
from django.contrib.auth.signals import user_logged_in
from allauth.socialaccount.models import SocialAccount
from dateutil.parser import isoparse
from django.db.models.signals import post_save
from django.dispatch import receiver
from django_apscheduler.jobstores import DjangoJobStore
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.date import DateTrigger
from django.db.models import Q
from .models import Client, Message


tz = pytz.timezone(settings.TIME_ZONE)


def send_message(broadcast_instance):
    clients = Client.objects.filter(Q(tag=broadcast_instance.tag_filter) |
                                    Q(operator_code=broadcast_instance.op_code_filter))
    for client in clients:
        print(f"Sending to: phone: {client.phone} - text: {broadcast_instance.text}...")
        message = Message.objects.create(broadcast=broadcast_instance, recieving_client=client)
        url = f'https://probe.fbrq.cloud/v1/send/{message.id}'
        body_data = {
            "id": message.id,
            "phone": message.recieving_client.phone,
            "text": message.broadcast.text
        }
        jwt_token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MjE4MDU0MjgsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6Imh0dHBzOi8vdC5tZS9rdWJhbmVtaWwifQ.SJEv_9GHwsNS0KeND7yMuj1l9Sf4jh8C1KEmXACV3KM'
        headers = {
            "Authorization": f"Bearer {jwt_token}",
            "Content-Type": "application/json"
        }
        response = requests.post(url, json=body_data, headers=headers)
        if response.status_code == 200:
            print(f"Succesfully sent to: phone: {client.phone} - text: {broadcast_instance.text}!")
            message.is_sent = True
            message.time = datetime.now(tz=tz)
            message.save()


@receiver(post_save, sender='api.Broadcast')
def schedule_broadcast(instance, created, **kwargs):
    if created:
        start_time = instance.start_time
        end_time = instance.end_time
        if type(start_time) == str:
            start_time = isoparse(start_time)
        if type(end_time) == str:
            end_time = isoparse(end_time)
        start_time = start_time.astimezone()
        end_time = end_time.astimezone()
        current_time = datetime.now(tz=tz)

        if start_time <= current_time <= end_time:
            send_message(instance)
        elif start_time > current_time:
            scheduler = BackgroundScheduler()
            # Add DjangoJobStore as the job store
            scheduler.add_jobstore(DjangoJobStore(), 'default')
            # Create a DateTrigger with the scheduled datetime
            trigger = DateTrigger(run_date=start_time)
            # Schedule the task with the DateTrigger and pass the Broadcast instance ID as an argument
            scheduler.add_job(send_message, args=[instance], trigger=trigger)
            scheduler.start()

@receiver(user_logged_in)
def make_user_staff(sender, request, user, **kwargs):
    try:
        social_account = SocialAccount.objects.get(user=user, provider='google')
        user.is_staff = True
        user.is_superuser = True
        user.save()
    except SocialAccount.DoesNotExist:
        print('logged in with Google')