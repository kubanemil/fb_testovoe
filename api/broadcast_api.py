from . import schemas
from ninja import Form
from typing import List
from .models import Broadcast
from ninja.router import Router


api = Router(tags=['Broadcast'])


@api.get("/", response={200: List[schemas.BroadcastSchema], 500: schemas.ErrorSchema})
def get_broadcast(request):
    """
    Возвращает все экземпляры модели Рассылки
    """
    try:
        return 200, Broadcast.objects.all()
    except Exception as e:
        return 500, {
            'message': 'Failed to fetch broadcasts.',
            'error': str(e)
        }


@api.post("/create", response={200: schemas.ModelReturnSchema, 500: schemas.ErrorSchema})
def create_broadcast(request, payload: schemas.BroadcastInputSchema = Form(...)):
    """
    Создает экземпляр модели Рассылки
    """
    try:
        b = Broadcast.objects.create(**payload.dict())
        b.full_clean()
        return 200, {
            'instance_info': payload.dict()
        }
    except Exception as e:
        return 500, {
            'message': 'Failed to create a broadcast',
            'error': str(e)
        }


@api.put('/update/{pk}', response={200: schemas.ModelReturnSchema, 500: schemas.ErrorSchema})
def update_broadcast(request, pk, updated_payload: schemas.BroadcastInputSchema):
    """
    Обновляет экземпляр модели Рассылки
    """
    try:
        broadcast = Broadcast.objects.get(pk=pk)
    except Exception as e:
        return 500, {"message": "Broadcast with the specified ID doesn't exists!",
                     "error": str(e)}

    for attr, value in updated_payload.dict().items():
        setattr(broadcast, attr, value)
    try:
        broadcast.full_clean()
    except Exception as e:
        return 500, {"message": "Failed to create a broadcast",
                     "error": str(e)}
    broadcast.save()
    return 200, {
            "instance_info": updated_payload.dict()}


@api.delete('/delete/{pk}', response={200: schemas.ModelReturnSchema, 500: schemas.ErrorSchema})
def delete_broadcast(request, pk: int):
    """
    Удаляет экземпляр модели Рассылки
    """
    try:
        broadcast = Broadcast.objects.get(pk=pk)
        broadcast_dict = schemas.BroadcastSchema.from_orm(broadcast)
        broadcast.delete()
        return 200, {"instance_info": broadcast_dict}
    except Exception as e:
        return 500, {"message": "No broadcast with ID {broadcast_id}.",
                     "error": str(e)}


@api.get("/stats/{broadcast_id}", response={200: schemas.BroadcastStatsSchema, 500: schemas.ErrorSchema})
def get_stats(request, broadcast_id: int):
    """
    Общая статистика о рассылке
    """
    try:
        broadcast = Broadcast.objects.get(pk=broadcast_id)
        if broadcast.messages.exists():
            message_count = broadcast.messages.count()
            sent_message_count = broadcast.messages.filter(is_sent=True).count()
            return 200, {
                'broadcast': schemas.BroadcastSchema.from_orm(broadcast),
                'message_count': message_count,
                'sent_message_count': sent_message_count}
        return 200, {
            'broadcast': schemas.BroadcastSchema.from_orm(broadcast),
            'message_count': 0,
            'sent_message_count': 0
        }
    except Exception as e:
        return 500, {"message": f"No broadcast with ID {broadcast_id}.",
                "error": str(e)}


@api.get("/{broadcast_id}/messages/", response={200: List[schemas.MessageSchema], 500: schemas.ErrorSchema})
def get_detailed_stats(request, broadcast_id: int):
    """
    Детальная статистика о рассылке и о сообщениях
    """
    stats = Broadcast.objects.get(id=broadcast_id).messages
    return stats