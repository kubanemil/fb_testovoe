from ninja.router import Router
from . import schemas
from ninja import Form
from api.models import Client
from typing import List

api = Router(tags=['Client'])


@api.get('/', response={200: List[schemas.ClientSchema], 500: schemas.ErrorSchema})
def get_clients(request):
    """
    Возвращает все экземпляры модели Клиента
    """
    try:
        return 200, Client.objects.all()
    except Exception as e:
        return 500, {
            'message': 'Failed to fetch clients.',
            'error': str(e)
        }


@api.post('/create', response={200: schemas.ModelReturnSchema, 500: schemas.ErrorSchema})
def create_client(request, payload: schemas.ClientInputSchema = Form(...)):
    """
    Создает экземпляр модели Клиента
    """
    try:
        c = Client.objects.create(**payload.dict())
        c.full_clean()
        return 200, {
            'instance_info': payload.dict()
        }
    except Exception as e:
        return 500, {
            'message': 'Failed to create a client',
            'error': str(e)
        }


@api.put('/update/{pk}', response={200: schemas.ModelReturnSchema, 500: schemas.ErrorSchema})
def update_client(request, pk, updated_payload: schemas.ClientUpdateSchema):
    """
    Обновляет экземпляр модели Клиента
    """
    try:
        client = Client.objects.get(pk=pk)
    except Exception as e:
        return 500, {"message": "Client with the specified ID doesn't exists!",
                     "error": str(e)}
    print(updated_payload.dict())
    for attr, value in updated_payload.dict().items():
        setattr(client, attr, value)
    try:
        client.full_clean()
    except Exception as e:
        return 500, {"message": "Failed to create a client",
                     "error": str(e)}
    client.save()
    return 200, {
            "instance_info": updated_payload.dict()}


@api.delete('/delete/{pk}', response={200: schemas.ModelReturnSchema, 500: schemas.ErrorSchema})
def delete_client(request, pk):
    """
    Удаляет экземпляр модели Клиента
    """
    try:
        client = Client.objects.get(pk=pk)
        client_dict = schemas.ClientSchema.from_orm(client)
        client.delete()
        return 200, {"instance_info": client_dict}
    except Exception as e:
        return 500, {"message": "Client with the specified ID doesn't exists!",
                     "error": str(e)}

