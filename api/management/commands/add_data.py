from django.core.management.base import BaseCommand
from create_data import *


class Command(BaseCommand):
    help = 'My custom command description.'

    def handle(self, *args, **options):
        create_superuser()
        create_clients()
        create_broadcasts()
        create_social()

