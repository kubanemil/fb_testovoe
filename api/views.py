from django.shortcuts import redirect, render


def redirect_main(request):
    return redirect('/docs')


def login_page(request):
    return render(request, 'custom/login.html')
