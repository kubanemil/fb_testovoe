from django.db import models


# Create your models here.
class Broadcast(models.Model):
    start_time = models.DateTimeField()
    text = models.TextField()
    tag_filter = models.CharField(max_length=100, null=True, blank=True)
    op_code_filter = models.CharField(max_length=100, null=True, blank=True)
    end_time = models.DateTimeField()

    @property
    def start_time_str(self):
        return str(self.start_time.astimezone())

    @property
    def end_time_str(self):
        return str(self.end_time.astimezone())

    class Meta:
        unique_together = ('start_time', 'end_time', 'text', 'tag_filter', 'op_code_filter')

    def __str__(self):
        return f"{self.start_time.strftime('%d/%m/%Y')}-{self.end_time.strftime('%d/%m/%Y')}|{self.tag_filter}-{self.op_code_filter}"


class Client(models.Model):
    phone = models.CharField(max_length=10, unique=True)
    operator_code = models.CharField(max_length=10)
    tag = models.CharField(max_length=100)
    gmt_timezone = models.IntegerField(default=3)

    @property
    def full_phone_number(self):
        return f"+7{self.phone}"

    def __str__(self):
        return f"{self.full_phone_number}-{self.tag}-{self.operator_code}"


class Message(models.Model):
    time = models.DateTimeField(auto_now=True)
    is_sent = models.BooleanField(default=False)
    broadcast = models.ForeignKey('Broadcast', related_name='messages', on_delete=models.CASCADE)
    recieving_client = models.ForeignKey('Client', on_delete=models.CASCADE)

    @property
    def time_str(self):
        return str(self.time.astimezone())

    @property
    def client_id(self):
        return self.recieving_client.id

    @property
    def broadcast_id(self):
        return self.broadcast.id

    def __str__(self):
        return f"{self.recieving_client}, {self.broadcast}"

