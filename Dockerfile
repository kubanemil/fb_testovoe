FROM python:3.10

WORKDIR .

COPY ./requirements.txt ./

RUN pip install --no-cache-dir -r requirements.txt

COPY . .

RUN python manage.py makemigrations
RUN python manage.py migrate
RUN python manage.py add_data


CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
CMD ["pytest", "tests/"]
