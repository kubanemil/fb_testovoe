import os

import pytz
from django.conf import settings
from django.contrib.sites.models import Site
from api.models import Broadcast, Client
from django.contrib.auth import get_user_model
from random import randint, choice
from datetime import datetime
from datetime import timedelta

def random_text():
    letters = "qwertyuiopasdfghjklzxcvbnmQWERYIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm"
    text = ""
    for i in range(randint(4, 10)):
        word = ""
        for j in range(randint(4, 10)):
            word += letters[randint(0, len(letters)-1)]
        word += " "
        text += word
    return text


tags = ['Male', 'Female', 'Rich', 'Poor']
op_codes = ['301', '554', '110', '321']
tz = pytz.timezone(settings.TIME_ZONE)


def create_broadcasts(num=5):
    for i in range(num):
        random_start_time = datetime.now(tz=tz)-timedelta(days=randint(30, 100))
        random_end_time = datetime.now(tz=tz) + timedelta(days=randint(1, 100))
        Broadcast.objects.create(start_time=random_start_time,
                                 text=random_text(),
                                 tag_filter=choice(tags),
                                 op_code_filter=choice(op_codes),
                                 end_time=random_end_time)


def create_test_broadcasts():
    start_time = datetime.now(tz=tz)+timedelta(minutes=1)
    end_time = datetime.now(tz=tz) + timedelta(minutes=11)
    Broadcast.objects.create(start_time=start_time,
                             text='HI BITCH!!!',
                             tag_filter=choice(tags),
                             op_code_filter=choice(op_codes),
                             end_time=end_time)


def create_clients(num=5):
    for i in range(num):
        random_phone = randint(1000000000, 9999999999)
        random_gmt = randint(-12, 12)
        Client.objects.create(phone=random_phone,
                              operator_code=choice(op_codes),
                              tag=choice(tags),
                              gmt_timezone=random_gmt)


def create_social(domain='127.0.0.1:8000', name='local'):
    Site.objects.all().delete()
    site = Site.objects.create(domain=domain, name=name)
    site.id = 1
    # site.save()


def create_superuser(username='emil', password='emil'):
    print("SUPERUSER CREATION!!!")
    get_user_model().objects.create_superuser(username=username, password=password).save()

