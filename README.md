# Установка


## Запуск через **docker-compose**

1. Откланируйте репу:
```shell
git clone https://gitlab.com/kubanemil/fb_testovoe
cd fb_testovoe
```
2. Запустите через docker-compose (тестирование происходит вместе с запуском):
```shell
docker-compose up
```

3. Идите в http://127.0.0.1 чтобы увидеть документацию OpenAPI 


## Запуск **вручную**
1. Откланируйте репу:
```shell
git clone https://gitlab.com/kubanemil/fb_testovoe
cd fb_testovoe
```
2. Создайте venv:
```shell
python3 -m venv venv
source venv/bin/activate
```
3. Установите requirements:
```shell
pip install -r requirements.txt
```
4. Добавьте данные (необязательно):
```shell
python3 add_data.py
```
5. Запустите сервер:
```shell
python3 manage.py runserver
```
6. Идите в http://127.0.0.1 чтобы увидеть документацию OpenAPI 
7. Можно запустить тесты с помощью pytest:
```shell
pytest
```


## Обьяснение

Рассылка = Broadcast

Клиент = Client

Сообщение = Message

### Модель Рассылки

class Broadcast(models.Model):

    start_time = дата и время запуска рассылки
    text = текст сообщения для доставки клиенту
    tag_filter = фильтр по тегу
    op_code_filter = фильтр по коду мобильного оператора
    end_time = дата и время окончания рассылки

При создании рассылки, автоматически создается и отправляется сообщения по 
указанной логике в ТЗ через signals в **signals.py**

### Модель Клиента

class Client(models.Model):

    phone = номер телефона клиента
    operator_code = код мобильного оператора
    tag = тег (произвольная метка)
    gmt_timezone = часовой пояс

### Модель Сообщения

class Message(models.Model):

    time = дата и время создания (отправки)
    is_sent = статус отправки
    broadcast = рассылка, в рамках которой было отправлено сообщение
    recieving_client = клиент, которому отправили


## Выполненные дополнительные задания:

- Организовать тестирование написанного кода
- Подготовить docker-compose для запуска всех сервисов проекта одной командой
- Сделать так, чтобы по адресу /docs/ открывалась страница со Swagger UI и в нём отображалось описание разработанного API. Пример: https://petstore.swagger.io
- Реализовать администраторский Web UI для управления рассылками и получения статистики по отправленным сообщениям
- Обеспечить интеграцию с внешним OAuth2 сервисом авторизации для административного интерфейса.